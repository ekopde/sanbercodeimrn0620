import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";

export default function App({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.body}>
        <View style={styles.developerContainer}>
          <Image
            style={styles.developerPhoto}
            source={require("./assets/ekopras.jpg")}
          />
          <View style={styles.developerStat}>
            <Text style={styles.developerName}>Eko Prasetyo</Text>
            <Text style={styles.developerStack}>Full Stack Developer</Text>
          </View>
        </View>
        <View style={styles.sosmedContainer}>
          <Text style={styles.sosmedText}>Social Media</Text>
          <View style={styles.sosmedGroup}>
            <TouchableOpacity>
              <View style={styles.sosmedBox}>
                <MaterialCommunityIcons
                  name="facebook-box"
                  size={40}
                  style={styles.sosmedIcon}
                />
                <Text style={styles.sosmedName}>Leonardus.ep</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.sosmedBox}>
                <MaterialCommunityIcons
                  name="gitlab"
                  size={40}
                  style={styles.sosmedIcon}
                />
                <Text style={styles.sosmedName}>ekopde</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.sosmedBox}>
                <MaterialCommunityIcons
                  name="telegram"
                  size={40}
                  style={styles.sosmedIcon}
                />
                <Text style={styles.sosmedName}>@ekopde</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.developerContainer}>
          <Image
            style={styles.developerPhoto}
            source={require("./assets/ajiwang.jpg")}
          />
          <View style={styles.developerStat}>
            <Text style={styles.developerName}>Arigus Wahyu Nur Prabowo</Text>
            <Text style={styles.developerStack}>Full Stack Developer</Text>
          </View>
        </View>
        <View style={styles.sosmedContainer}>
          <Text style={styles.sosmedText}>Social Media</Text>
          <View style={styles.sosmedGroup}>
            <TouchableOpacity>
              <View style={styles.sosmedBox}>
                <MaterialCommunityIcons
                  name="facebook-box"
                  size={40}
                  style={styles.sosmedIcon}
                />
                <Text style={styles.sosmedName}>ariguswahyu</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.sosmedBox}>
                <MaterialCommunityIcons
                  name="gitlab"
                  size={40}
                  style={styles.sosmedIcon}
                />
                <Text style={styles.sosmedName}>ariguswahyu</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.sosmedBox}>
                <MaterialCommunityIcons
                  name="telegram"
                  size={40}
                  style={styles.sosmedIcon}
                />
                <Text style={styles.sosmedName}>@ariguswahyu</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={styles.tabBar}>
        <TouchableOpacity
          onPress={() => navigation.navigate("Home")}
          style={styles.tabItem}
        >
          <MaterialCommunityIcons
            name="home"
            size={25}
            style={{ color: "#3498DB" }}
          />
          <Text style={styles.tabTitle}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate("Info Bed")}
          style={styles.tabItem}
        >
          <MaterialCommunityIcons
            name="hospital-building"
            size={25}
            style={{ color: "#3498DB" }}
          />
          <Text style={styles.tabTitle}>Info Bed</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate("Info Antrian")}
          style={styles.tabItem}
        >
          <MaterialCommunityIcons
            name="medical-bag"
            size={25}
            style={{ color: "#3498DB" }}
          />
          <Text style={styles.tabTitle}>Info Antrian</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate("About")}
          style={styles.tabItem}
        >
          <MaterialCommunityIcons
            name="account"
            size={25}
            style={{ color: "#3498DB" }}
          />
          <Text style={styles.tabTitle}>About Us</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F1F1F1",
  },
  topNavigation: {
    marginTop: 23,
    marginLeft: 18,
    flexDirection: "row",
    alignItems: "center",
  },
  textNavigation: {
    fontWeight: "bold",
    fontSize: 18,
  },
  iconNavigation: {
    color: "#3498DB",
    backgroundColor: "white",
  },
  developerContainer: {
    marginTop: 10,
    backgroundColor: "#3498DB",
    width: 327,
    height: 88,
    borderRadius: 3,
    alignSelf: "center",
    flexDirection: "row",
  },
  developerPhoto: {
    marginLeft: 7,
    alignSelf: "center",
    width: 72,
    height: 72,
    borderRadius: 8,
  },
  developerStat: {
    paddingLeft: 20,
    alignSelf: "center",
  },
  developerName: {
    fontWeight: "bold",
    fontSize: 18,
    color: "white",
  },
  developerStack: {
    fontSize: 14,
    color: "white",
  },
  sosmedContainer: {
    marginTop: 10,
    backgroundColor: "#3498DB",
    width: 327,
    height: 177,
    borderRadius: 3,
    alignSelf: "center",
  },
  sosmedText: {
    margin: 10,
    paddingLeft: 10,
    fontSize: 18,
    color: "white",
    fontWeight: "bold",
  },
  sosmedGroup: {
    flexDirection: "row",
    alignSelf: "center",
  },
  sosmedBox: {
    backgroundColor: "#FFFFFF",
    width: 89,
    height: 98,
    borderRadius: 4,
    margin: 5,
  },
  sosmedIcon: {
    margin: 5,
    color: "#26CAC0",
    alignSelf: "center",
  },
  sosmedName: {
    margin: 20,
    fontSize: 10,
    color: "#26CAC0",
    alignSelf: "center",
  },
  body: {
    flex: 1,
  },
  tabBar: {
    backgroundColor: "white",
    height: 60,
    borderTopWidth: 0.5,
    borderColor: "#E5E5E5",
    flexDirection: "row",
    justifyContent: "space-around",
  },
  tabItem: {
    alignItems: "center",
    justifyContent: "center",
  },
  tabTitle: {
    paddingTop: 4,
    fontSize: 11,
    color: "#3c3c3c",
  },
});
