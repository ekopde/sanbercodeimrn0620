import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { Value } from "react-native-reanimated";

export default function App({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.body}>
        <TouchableOpacity onPress={() => navigation.navigate("Info Bed")}>
          <View style={styles.bedContainer}>
            <MaterialCommunityIcons
              name="bed-empty"
              size={40}
              style={styles.iconContainerLeft}
            />
            <View style={styles.textContainer}>
              <Text style={styles.h1Text}>Ketersediaan Kamar</Text>
              <Text style={styles.h2Text}>Informasi Kamar Perawatan</Text>
            </View>
            <MaterialCommunityIcons
              name="arrow-right-circle-outline"
              size={40}
              style={styles.iconContainerRight}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("Info Antrian")}>
          <View style={styles.bedContainer}>
            <MaterialCommunityIcons
              name="account-multiple"
              size={40}
              style={styles.iconContainerLeft}
            />
            <View style={styles.textContainer}>
              <Text style={styles.h1Text}>Antrian Poliklinik</Text>
              <Text style={styles.h2Text}>Informasi Antrian Poliklinik</Text>
            </View>
            <MaterialCommunityIcons
              name="arrow-right-circle-outline"
              size={40}
              style={styles.iconContainerRight}
            />
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate("Info Dokter")}>
          <View style={styles.bedContainer}>
            <MaterialCommunityIcons
              name="account-multiple"
              size={40}
              style={styles.iconContainerLeft}
            />
            <View style={styles.textContainer}>
              <Text style={styles.h1Text}>Daftar Dokter</Text>
              <Text style={styles.h2Text}>Informasi Dokter Yang Praktek di RS</Text>
            </View>
            <MaterialCommunityIcons
              name="arrow-right-circle-outline"
              size={40}
              style={styles.iconContainerRight}
            />
          </View>
        </TouchableOpacity>
        <View style={styles.exitBar}></View>
        <TouchableOpacity
          style={styles.exitButton}
          onPress={() => navigation.navigate("Login")}
        >
          <Text style={styles.exitText}>Keluar</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.tabBar}>
        <TouchableOpacity
          onPress={() => navigation.navigate("Home")}
          style={styles.tabItem}
        >
          <MaterialCommunityIcons
            name="home"
            size={25}
            style={{ color: "#3498DB" }}
          />
          <Text style={styles.tabTitle}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate("Info Bed")}
          style={styles.tabItem}
        >
          <MaterialCommunityIcons
            name="hospital-building"
            size={25}
            style={{ color: "#3498DB" }}
          />
          <Text style={styles.tabTitle}>Info Bed</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate("Info Antrian")}
          style={styles.tabItem}
        >
          <MaterialCommunityIcons
            name="medical-bag"
            size={25}
            style={{ color: "#3498DB" }}
          />
          <Text style={styles.tabTitle}>Info Antrian</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate("Info Dokter")}
          style={styles.tabItem}
        >
          <MaterialCommunityIcons
            name="account"
            size={25}
            style={{ color: "#3498DB" }}
          />
          <Text style={styles.tabTitle}>Dokter</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate("About")}
          style={styles.tabItem}
        >
          <MaterialCommunityIcons
            name="account"
            size={25}
            style={{ color: "#3498DB" }}
          />
          <Text style={styles.tabTitle}>About Us</Text>
        </TouchableOpacity>

      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F1F1F1",
  },
  bedContainer: {
    marginTop: 10,
    backgroundColor: "#FFFFFF",
    width: 328,
    height: 90,
    borderRadius: 8,
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "center",
  },
  iconContainerLeft: {
    color: "black",
    alignSelf: "center",
  },
  iconContainerRight: {
    color: "#3498DB",
    alignSelf: "center",
  },
  textContainer: {
    margin: 20,
    justifyContent: "center",
  },
  h1Text: {
    fontSize: 16,
    fontWeight: "bold",
  },
  h2Text: {
    fontSize: 12,
    color: "#9E9E9E",
  },

  body: {
    flex: 1,
  },
  tabBar: {
    backgroundColor: "white",
    height: 60,
    borderTopWidth: 0.5,
    borderColor: "#E5E5E5",
    flexDirection: "row",
    justifyContent: "space-around",
  },
  tabItem: {
    alignItems: "center",
    justifyContent: "center",
  },
  tabTitle: {
    paddingTop: 4,
    fontSize: 11,
    color: "#3c3c3c",
  },
  exitBar: {
    flex: 1,
  },
  exitButton: {
    marginBottom: 10,
    backgroundColor: "#3498DB",
    width: 328,
    height: 50,
    borderRadius: 8,
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "center",
  },
  exitText: {
    fontSize: 14,
    alignSelf: "center",
    color: "#FFFFFF",
  },
});
