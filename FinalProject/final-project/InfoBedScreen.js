import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import Axios from "axios";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false,
    };
  }
  componentDidMount() {
    this.getInfoBed();
  }
  getInfoBed = async () => {
    try {
      const response = await Axios.get(
        `https://finalsanber.firebaseio.com/tempattidur.json`
      );
      this.setState({ isError: false, isLoading: false, data: response.data });
    } catch (error) {
      this.setState({ isLoading: false, isError: true });
    }
  };

  render() {
    //  If load data
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: "center", justifyContent: "center", flex: 1 }}
        >
          <ActivityIndicator size="large" color="red" />
        </View>
      );
    }
    // If data not fetch
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: "center", justifyContent: "center", flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <FlatList
            data={this.state.data.item}
            renderItem={({ item }) => (
              <View style={styles.bedContainer}>
                <View style={styles.kosongContainer}>
                  <Text style={styles.textKosong}>Kosong</Text>
                  <Text style={styles.isiKosong}>{item.bedkosong}</Text>
                </View>
                <View style={styles.terisiContainer}>
                  <Text style={styles.textRuang}>{item.ruangan}</Text>
                  <Text style={styles.textDetail}>
                    Total Bed {item.totalbed}
                  </Text>
                  <Text style={styles.textDetail}>Terisi {item.bedterisi}</Text>
                  <Text style={styles.textDetail}>
                    Extra Bed {item.extrabed}
                  </Text>
                </View>
              </View>
            )}
            keyExtractor={({ id }, index) => index.toString()}
          />
        </View>
        <View style={styles.tabBar}>
          <TouchableOpacity
            // onPress={() => this.navigation.navigate("Home")}
            onPress={() => this.props.navigation.navigate("Home")}
            style={styles.tabItem}
          >
            <MaterialCommunityIcons
              name="home"
              size={25}
              style={{ color: "#3498DB" }}
            />
            <Text style={styles.tabTitle}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity
            // onPress={() => navigation.navigate("Info Bed")}
            onPress={() => this.props.navigation.navigate("Info Bed")}
            style={styles.tabItem}
          >
            <MaterialCommunityIcons
              name="hospital-building"
              size={25}
              style={{ color: "#3498DB" }}
            />
            <Text style={styles.tabTitle}>Info Bed</Text>
          </TouchableOpacity>
          <TouchableOpacity
            // onPress={() => navigation.navigate("Info Antrian")}
            onPress={() => this.props.navigation.navigate("Info Antrian")}
            style={styles.tabItem}
          >
            <MaterialCommunityIcons
              name="medical-bag"
              size={25}
              style={{ color: "#3498DB" }}
            />
            <Text style={styles.tabTitle}>Info Antrian</Text>
          </TouchableOpacity>
          <TouchableOpacity
            // onPress={() => navigation.navigate("About")}
            onPress={() => this.props.navigation.navigate("About")}
            style={styles.tabItem}
          >
            <MaterialCommunityIcons
              name="account"
              size={25}
              style={{ color: "#3498DB" }}
            />
            <Text style={styles.tabTitle}>About Us</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F1F1F1",
  },
  bedContainer: {
    marginTop: 10,
    backgroundColor: "#3498DB",
    width: 328,
    height: 110,
    borderRadius: 8,
    alignSelf: "center",
    flexDirection: "row",
  },
  kosongContainer: {
    marginLeft: 7,
    alignSelf: "center",
    width: 86,
    height: 86,
    borderRadius: 8,
    backgroundColor: "#FFFFFF",
    alignItems: "center",
  },
  textKosong: {
    fontSize: 14,
    color: "#26CAC0",
  },
  isiKosong: {
    fontSize: 48,
    fontWeight: "bold",
    color: "#26CAC0",
  },
  terisiContainer: {
    justifyContent: "center",
    margin: 10,
  },
  textRuang: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#FFFFFF",
  },
  textDetail: {
    fontSize: 14,
    fontWeight: "bold",
    color: "#FFFFFF",
  },
  body: {
    flex: 1,
  },
  tabBar: {
    backgroundColor: "white",
    height: 60,
    borderTopWidth: 0.5,
    borderColor: "#E5E5E5",
    flexDirection: "row",
    justifyContent: "space-around",
  },
  tabItem: {
    alignItems: "center",
    justifyContent: "center",
  },
  tabTitle: {
    paddingTop: 4,
    fontSize: 11,
    color: "#3c3c3c",
  },
});
