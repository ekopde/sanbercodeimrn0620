import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      password: "",
      isError: false,
    };
  }
  loginHandler() {
    if (this.state.password == "123456") {
      this.setState({ isError: false });
      this.props.navigation.navigate("Home", {
        userName: this.state.userName,
      });
    } else {
      this.setState({ isError: true });
    }
    console.log(
      this.state.userName,
      " ",
      this.state.password,
      " ",
      this.state.isError
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.LogoContainer}
          source={require("./assets/logo-pwdc.png")}
        />
        <Text style={styles.LoginText}>Login</Text>
        <View>
          <TextInput
            placeholder="Email"
            style={styles.InputText}
            onChangeText={(userName) => this.setState({ userName })}
          ></TextInput>
          <TextInput
            placeholder="Password"
            style={styles.InputText}
            onChangeText={(password) => this.setState({ password })}
            secureTextEntry={true}
          ></TextInput>
        </View>
        <Text
          style={this.state.isError ? styles.errorText : styles.hiddenErrorText}
        >
          Password Salah
        </Text>
        <TouchableOpacity
          style={styles.ButtonContainer}
          onPress={() => this.loginHandler()}
        >
          <Text style={styles.ButtonText}>Login</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  LogoContainer: {
    marginTop: 63,
    width: 300,
    height: 169,
    alignSelf: "center",
  },
  LoginText: {
    marginTop: 50,
    fontSize: 24,
    alignSelf: "center",
    fontWeight: "bold",
  },
  InputText: {
    alignSelf: "center",
    marginTop: 20,
    height: 50,
    width: 327,
    borderColor: "#C4C4C4",
    borderWidth: 1,
    backgroundColor: "#FFFFFF",
    borderRadius: 3,
  },
  ButtonContainer: {
    marginTop: 46,
    width: 327,
    height: 60,
    backgroundColor: "#3498DB",
    alignSelf: "center",
    justifyContent: "center",
    borderRadius: 3,
  },
  ButtonText: {
    fontSize: 16,
    alignSelf: "center",
    color: "#FFFFFF",
  },
  errorText: {
    color: "red",
    textAlign: "center",
    marginTop: 20,
    fontWeight: "bold",
  },
  hiddenErrorText: {
    color: "transparent",
    textAlign: "center",
    marginTop: 20,
    fontWeight: "bold",
  },
});
