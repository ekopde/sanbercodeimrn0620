import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import "react-native-gesture-handler";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import LoginScreen from "./LoginScreen";
import AboutScreen from "./AboutScreen";
import InfoBedScreen from "./InfoBedScreen";
import PoliScreen from "./AntrianPoli";
import HomeScreen from "./Home";
import DokterScreen from "./DaftarDokter";

const Stack = createStackNavigator();

export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen
            name="Login"
            component={LoginScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{ headerShown: true }}
            options={({ navigation }) => ({
              headerLeft: () => (
                <MaterialCommunityIcons
                  name="shield-home"
                  size={30}
                  color="black"
                  style={{ marginLeft: 20 }}
                />
              ),
            })}
          />
          <Stack.Screen
            name="About"
            component={AboutScreen}
            options={{ headerShown: true }}
          />
          <Stack.Screen
            name="Info Bed"
            component={InfoBedScreen}
            options={{ headerTitle: "Info Bed" }}
            options={{ headerShown: true }}
          />
          <Stack.Screen
            name="Info Antrian"
            component={PoliScreen}
            options={{ headerTitle: "Info Antrian" }}
            options={{ headerShown: true }}
          />
          <Stack.Screen
            name="Info Dokter"
            component={DokterScreen}
            options={{ headerTitle: "Info Dokter" }}
            options={{ headerShown: true }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
