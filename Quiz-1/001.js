// a
function bandingkan(num1, num2=-1) {
    // code di sini
    var nilai = 0
    if( Math.sign(num1)==1 && Math.sign(num2)!==1 ) {
      nilai = num1
  
    } else if(Math.sign(num1)==1 && Math.sign(num2)==1) {
      if ( +num1 > +num2 ) {
        nilai = num1
  
      } else if (num1 == num2) {
        nilai = -1
  
      } else {
        nilai = num2
  
      }
  
    } else {
      nilai = -1
  
    }
  
    return nilai
  }
  // TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18
  // b
  function balikString(kata) {
    // Silakan tulis code kamu di sini
    var kebalik = ""
    for (var i = kata.length - 1; i >= 0 ; i--) {
      kebalik += kata[i]
    }
    return kebalik  
  }
  // TEST CASES BalikString
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah
// c
  function palindrome(kata) {
    // Silakan tulis code kamu di sini
    var cekpal = balikString(kata)
    var status = ""
    if(kata==cekpal ) {
      status = true
    }else{
      status = false
    }
  
    return status
  }
  // TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false
