import React, { Component } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
//import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { MaterialCommunityIcons } from "@expo/vector-icons";
export default class BodySkill extends Component {
    render() {
        let skill = this.props.data;
        // alert(skill)
        return (
            <View style={styles.container}>
                {/* { skill.items.map( (item) =>)}
                <View style={styles.boxLarge}>
                    <Text style={styles.text}> {skill.skillName} </Text>
                </View> */}
                { skill.items.map((item) => (
                    <View  style={styles.boxLarge}>
                        <View style={{ flexDirection: 'row', justifyContent: 'Space-around'  }}>
                            <Icon name={item.iconName} size={90} style={{color: '#003366'}} />
                            <View style={styles.logoUser}>
                                <Text style={{fontSize:24 }} >{item.skillName}</Text>
                                <Text style={{fontSize:16,  color: '#003366' }} >{item.categoryName}</Text>
                                <Text style={{fontSize:48, fontWeight: 'bold', color: '#FFFFFF', alignSelf: 'flex-end' }} >{item.percentageProgress}</Text>
                            </View>
                            <View style={styles.logoUser}>
                                <Icon name="chevron-right" size={100} style={{color: '#003366'}} />
                            </View>
                        </View>
                    </View>
                ))}
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    boxLarge: {
        justifyContent: 'center',
        width: '100%',
        height: 170,
        marginBottom: 10,
        marginRight: 10,
        backgroundColor: 'steelblue',
        borderRadius: 4,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        
        elevation: 10,    
      },
      text: {
        fontSize: 16,
        color: '#606070',
        padding: 10,
      },      
})  
