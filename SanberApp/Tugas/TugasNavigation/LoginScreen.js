import React from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  TextInput,
  TouchableOpacity,
} from "react-native";

export default class LoginPage extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topBar}>
          <Image
            source={require("./assets/logo.png")}
            style={{ width: 375, height: 116 }}
          />
        </View>
        <View style={styles.middleBar}>
          <Text style={{ color: "#003366", fontSize: 24 }}>Login</Text>
        </View>
        <View style={styles.loginBar}>
          <Text style={styles.textDetail}>Username/Email</Text>
          <TextInput style={styles.inputText}></TextInput>
          <Text style={styles.textDetail}>Password</Text>
          <TextInput style={styles.inputText}></TextInput>
        </View>
        <View style={styles.buttonBar}>
          <TouchableOpacity
            style={styles.loginButton}
            onPress={() => this.props.navigation.navigate("Welcome")}
          >
            <Text style={{ color: "white", fontSize: 24 }}> Masuk </Text>
          </TouchableOpacity>
          <Text style={{ color: "#3EC6FF", fontSize: 24 }}>atau</Text>
          <TouchableOpacity style={styles.registerButton}>
            <Text style={{ color: "white", fontSize: 24 }}> Daftar ? </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  topBar: {
    backgroundColor: "white",
    paddingHorizontal: 15,
    alignItems: "center",
  },
  middleBar: {
    paddingTop: 30,
    alignItems: "center",
  },
  loginBar: {
    paddingTop: 30,
  },
  inputText: {
    height: 48,
    width: 294,
    borderColor: "#003366",
    borderWidth: 1,
    backgroundColor: "#FFFFFF",
  },
  buttonBar: {
    paddingTop: 30,
    alignItems: "center",
  },
  loginButton: {
    width: 140,
    backgroundColor: "#3EC6FF",
    borderRadius: 16,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  registerButton: {
    width: 140,
    backgroundColor: "#003366",
    borderRadius: 16,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  textDetail: {
    color: "#003366",
    fontSize: 16,
    padding: 5,
  },
});
