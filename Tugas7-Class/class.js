//soal 1
class Animal {
	constructor( name ) {
		this.name = name
		this.legs = 4
		this.cold_blooded = false
	}

}

class Frog extends Animal {
	constructor(name) {
		super(name)
		this.cold_blooded = true
	}

	jump() {
		return "hop hop"
	}

}

class Ape extends Animal {
	constructor(name) {
		super(name)
		this.legs = 2
	}

	yell() {
		return "Auooo"
	}
}

//soal 2
class Clock {
    constructor( temp ) {
    	this.template = temp.template
    	this.timer = undefined
    }

    render() {
	    var date 	  = new Date();

	    var hours = date.getHours();
	    if (hours < 10) hours = '0' + hours;

	    var mins = date.getMinutes();
	    if (mins < 10) mins = '0' + mins;

	    var secs = date.getSeconds();
	    if (secs < 10) secs = '0' + secs;

	    var output = this.template
	    	.replace('h', hours)
	    	.replace('m', mins)
	    	.replace('s', secs);

	    console.log(output);
    }

    stop() {
    	clearInterval(this.timer);
    }

    start() {
	    this.render()
	    this.timer = setInterval( this.render.bind(this) , 1000);
	}
}



var sheep = new Animal("shaun");
 
console.log("1. Animal Class")
console.log("Release 0")
console.log("---------------------------------")
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
console.log("\n")

console.log("Release 1")
console.log("---------------------------------")
var sungokong = new Ape("kera sakti")
console.log( sungokong.name ) // kera sakti
console.log( sungokong.legs ) // 2
console.log( sungokong.cold_blooded ) // false
console.log( sungokong.yell() ) // "Auooo"
console.log("\n")

var kodok = new Frog("buduk")
console.log( kodok.name ) // buduk
console.log( kodok.legs ) // 4
console.log( kodok.cold_blooded ) // true
console.log( kodok.jump() ) // "hop hop" 
console.log("\n")

console.log("2. Function to Class")
console.log("---------------------------------")
var clock = new Clock({template: 'h:m:s'});
clock.start();
